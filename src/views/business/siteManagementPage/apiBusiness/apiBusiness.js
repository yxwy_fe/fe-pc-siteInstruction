import request from '@/utils/request';

/**站点管理*/
export function getStationListApi(data) {
	return request({
		url: '/system/media/stationList',
		method: 'get',
		params: data
	})
}

/**编辑站点*/
export function getUpdateStationApi(data) {
	return request({
		url: '/system/media/updateStation',
		method: 'post',
		data: data
	})
}

/**开启/关闭站点上传*/
export function getUpdateStationStatusApi(data) {
	return request({
		url: '/system/media/updateStationStatus',
		method: 'post',
		data: data
	})
}

