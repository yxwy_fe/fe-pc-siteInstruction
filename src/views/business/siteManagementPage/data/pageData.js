const pageData = {
	/*搜索表单*/
	searchConfig: {
		/**ip地址*/
		ip: "",

		/**站点名称*/
		stationName: "",

		pageNum: "1",

		/**数量*/
		pageSize: "10",
	},

	/**列表数据*/
	listData: [],

	/**总条数*/
	total: 0,

	/**加载*/
	loading: true,

	/*弹框*/
	formConfig: {
		isShow: false,
		dialogTitle: "编辑",
		form: {
			/**主键ID*/
			id: "",

			/**所属部门ID*/
			groupId: "",

			/**所属单位*/
			groupName: "",

			/**站点编码*/
			number: "",

			/**IP地址*/
			ip: "",

			/**联系人*/
			linkman: "",

			/**联系方式*/
			phone: "",

			/**描述*/
			remark: "",

			/**站点名称*/
			stationName: "",

			/**单位编码*/
			deptId: ""
		}
	}
}

export default pageData
