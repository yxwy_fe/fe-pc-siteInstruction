import pageData from '../data/pageData.js';
import {getStationListApi} from '../apiBusiness/apiBusiness';
import {Message} from "element-ui";


class InitPageData {
	static async init() {
		/**初始化列表*/
		await InitPageData.getListData()
	}

	static async getListData() {
		let listRes = await getStationListApi(pageData.searchConfig);
		console.log('listRes', listRes)
		if (listRes && listRes.code === 200) {
			pageData.loading = false;
			pageData.listData = listRes.rows;
			pageData.total = listRes.total;

		} else {
			pageData.loading = false;
			Message.error("列表服务异常");
		}
	}
}

export default InitPageData;
