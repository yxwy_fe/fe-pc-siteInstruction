import pageData from '../data/pageData.js';
import {getUpdateStationApi, getUpdateStationStatusApi} from '../apiBusiness/apiBusiness.js';
import InitPageData from '../business/InitPageData';
import {Message} from "element-ui";
import {MessageBox} from "element-ui";

/**编辑按钮*/
export function compileBusiness(row) {
	cancelBusiness()
	pageData.formConfig.form.id = row.id;
	pageData.formConfig.form.groupId = row.groupId;
	pageData.formConfig.form.groupName = row.groupName;
	pageData.formConfig.form.number = row.number;
	pageData.formConfig.form.ip = row.ip;
	pageData.formConfig.form.linkman = row.linkman;
	pageData.formConfig.form.phone = row.phone;
	pageData.formConfig.form.remark = row.remark;
	pageData.formConfig.form.stationName = row.stationName;
	pageData.formConfig.form.deptId = row.deptId;
	pageData.formConfig.isShow = true
}

/**取消按钮*/
export function cancelBusiness() {
	pageData.formConfig.form.id = '';
	pageData.formConfig.form.groupId = '';
	pageData.formConfig.form.groupName = '';
	pageData.formConfig.form.number = '';
	pageData.formConfig.form.ip = '';
	pageData.formConfig.form.linkman = '';
	pageData.formConfig.form.phone = '';
	pageData.formConfig.form.remark = '';
	pageData.formConfig.form.stationName = '';
	pageData.formConfig.form.deptId = '';
	pageData.formConfig.isShow = false;
}

/**保存按钮*/
export async function saveBusiness() {
	let saveRes = await getUpdateStationApi(pageData.formConfig.form);
	console.log('saveRes', saveRes)
	if (saveRes && saveRes.code === 200) {
		cancelBusiness();
		pageData.loading = true;
		await InitPageData.getListData();
	} else {
		Message.error("编辑服务异常");
	}

}

/**开启&关闭按钮*/
export function switchBusiness(row, val) {
	let title = '';
	if (val === 2) {
		title = '开启'
	} else if (val === 0) {
		title = '关闭'
	}

	MessageBox.confirm('确定要' + title + '?', '温馨提示', {
		confirmButtonText: '确定', cancelButtonText: '取消', type: 'warning'
	}).then(async () => {
		let switchRes = await getUpdateStationStatusApi({
			id: row.id, status: String(val)
		})
		if (switchRes && switchRes.code === 200) {
			pageData.loading = true;
			await InitPageData.getListData();
		} else {
			Message.error(title + "服务异常");
		}
	}).catch(() => {
		this.$message({
			type: 'info',
			message: '已取消' + title,
		});
	});
}
