import request from '@/utils/request';

/**拍摄时间统计*/
export function getVideoStaticsApi(data) {
	return request({
		url: '/system/media/videoStatics',
		method: 'post',
		data: data
	})
}

