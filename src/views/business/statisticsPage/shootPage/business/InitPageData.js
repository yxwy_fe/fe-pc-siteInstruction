import pageData from '../data/pageData.js'
import {getVideoStaticsApi} from '../apiBusiness/apiBusiness.js'
import {Message} from "element-ui";


class InitPageData {
	static async init() {
		await InitPageData.getListData()
	}

	static async getListData() {
		let listRes = await getVideoStaticsApi(pageData.searchConfig);
		if (listRes && listRes.code === 200) {
			pageData.loading = false;
			console.log('listRes', listRes);
			pageData.listData = listRes.data.records;
			pageData.total = listRes.data.total;

			console.log('pageData.listData', pageData.listData)
		} else {
			pageData.loading = false;
			Message.error("列表服务异常");
		}
	}
}

export default InitPageData;
