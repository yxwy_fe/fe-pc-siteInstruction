const pageData = {
	/*搜索表单*/
	searchConfig: {
		/**警号*/
		policeNumber: "",

		/**设备号*/
		deviceNumber: "",

		/**拍摄时段开始时间*/
		startTime: "",

		/**拍摄时段结束时间*/
		endTime: "",

		/**页码*/
		pageNum: "1",

		/**数量*/
		pageSize: "10",
	},

	/**列表数据*/
	listData: [],

	/**拍摄时间*/
	shootTime: '',

	/**总调数*/
	total: 0,

	/**加载*/
	loading: true,
}

export default pageData
