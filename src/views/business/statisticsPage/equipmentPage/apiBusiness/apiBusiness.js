import request from '@/utils/request';

/**设备统计*/
export function getDevStaticsApi(data) {
	return request({
		url: '/system/media/devStatics',
		method: 'post',
		data: data
	})
}

