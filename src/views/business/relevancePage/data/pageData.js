const pageData = {
	/*搜索表单*/
	searchConfig: {
		/**页码*/
		pageNum: "1",

		/**数量*/
		pageSize: "10",
	},

	/**列表数据*/
	listData: [],

	/**案发时间*/
	incidentTime: '',
}

export default pageData
