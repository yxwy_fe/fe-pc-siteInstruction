import pageData from '../data/pageData.js'
import {getErrorFileListApi} from '../apiBusiness/apiBusiness.js'
import {Message} from "element-ui";

class InitPageData {
	static async init() {
		/**列表初始化*/
		await InitPageData.getListData()
	}

	static async getListData() {
		let listRes = await getErrorFileListApi(pageData.searchConfig)
		console.log('listRes', listRes)
		if (listRes && listRes.code === 200) {
			pageData.loading = false;
			console.log('listRes', listRes);
			pageData.listData = listRes.rows;
			pageData.total = listRes.total;
		} else {
			pageData.loading = false;
			Message.error("列表服务异常");
		}
	}

}

export default InitPageData;
