import request from '@/utils/request';

/**开启/关闭站点上传*/
export function getErrorFileListApi(data) {
	return request({
		url: '/system/media/errorFileList',
		method: 'get',
		params: data
	})
}

