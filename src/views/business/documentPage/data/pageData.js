const pageData = {
	/*搜索表单*/
	searchConfig: {
		/**文件名称*/
		fileName: "",

		/**IP地址*/
		ip: "",

		/**站点编号*/
		stationNumber: '',

		/**开始时间*/
		startTime: '',

		/**结束时间*/
		endTime: '',

		/**页码*/
		pageNum: "1",

		/**数量*/
		pageSize: "10",
	},

	/**列表数据*/
	listData: [],

	/**总条数*/
	total: 0,

	/**加载*/
	loading: true,

	/*时间*/
	dataTime: ''
}

export default pageData
