const pageData = {
	/*搜索表单*/
	searchConfig: {
		/**警号*/
		policeNumber: "",

		/**姓名*/
		userName: "",

		/**设备号*/
		deviceNumber: "",

		/**拍摄开始时间*/
		createTimeStart: "",

		/**拍摄结束时间*/
		createTimeEnd: "",

		/**采集开始时间*/
		importTimeStart: "",

		/**采集结束时间*/
		importTimeEnd: "",

		/**视频标记 1-普通 3-重点*/
		"level": "",

		/*类型 video-视频 photo-图片 audio-音频*/
		type: "",

		/**页码*/
		pageNum: "1",

		/**数量*/
		pageSize: "10",
	},

	/**总条数*/
	total: 0,

	/**列表数据*/
	listData: [],

	/**拍摄时间*/
	shootTime: '',

	/**采集时间*/
	gatherTime: '',

	/**视频标记*/
	signType: [{
		id: 0, value: "", label: "全部",
	}, {
		id: 1, value: "1", label: "普通",
	}, {
		id: 3, value: "3", label: "重点",
	},],

	/**视频标记*/
	type: [{
		id: 0, value: "video", label: "视频",
	}, {
		id: 1, value: "photo", label: "图片",
	}, {
		id: 3, value: "audio", label: "音频",
	},],

	/**加载*/
	loading: true,

	/*详情表单*/
	formConfig: {
		isShow: false, dialogTitle: "", url: '', type: ''
	},
}

export default pageData
