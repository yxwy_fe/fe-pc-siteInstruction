import pageData from '../data/pageData'
import {getMediaListApi} from '../apiBusiness/apiBusiness'
import {Message} from "element-ui";

class InitPageData {
	static async init() {

		/**列表初始化*/
		await InitPageData.getListData();
	}

	/**列表初始化*/
	static async getListData() {
		let listRes = await getMediaListApi(pageData.searchConfig)
		if (listRes && listRes.code === 200) {
			pageData.loading = false;
			console.log('listRes', listRes);
			pageData.listData = listRes.rows;
			pageData.total = listRes.total;
		} else {
			pageData.loading = false;
			Message.error("列表服务异常");
		}
	}

}

export default InitPageData;
