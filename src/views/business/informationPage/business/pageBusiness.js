import pageData from '../data/pageData';
import InitPageData from '../business/InitPageData'


/** 搜索方法*/
export async function searchBusiness() {
	// console.log("searchBusiness，搜索方法");
	pageData.loading = true;
	pageData.searchConfig.pageNum = '1';
	pageData.searchConfig.pageSize = '10';
	await InitPageData.getListData();
}

/** 重置搜索条件方法*/
export async function resetSearchFormBusiness() {
	pageData.loading = true;
	pageData.searchConfig.policeNumber = '';
	pageData.searchConfig.userName = '';
	pageData.searchConfig.deviceNumber = '';
	pageData.searchConfig.createTimeStart = '';
	pageData.searchConfig.createTimeEnd = '';
	pageData.searchConfig.importTimeStart = '';
	pageData.searchConfig.importTimeEnd = '';
	pageData.searchConfig.level = '';
	pageData.searchConfig.type = '';
	pageData.searchConfig.pageNum = '1';
	pageData.searchConfig.pageSize = '10';
	pageData.shootTime = '';
	pageData.gatherTime = '';
	await InitPageData.getListData();
}


/**分页：改变每页条数*/
export async function sizeChangeBusiness(res) {
	pageData.loading = true;
	// console.log("改变分页条数，sizeChangeBusiness", res)
	pageData.searchConfig.pageNum = "1";
	pageData.searchConfig.pageSize = res;
	await InitPageData.getListData();
}

/** 分页：点击对应页面*/
export async function currentChangeBusiness(res) {
	// console.log("改变分页页数，currentChangeBusiness", res)
	pageData.loading = true;
	pageData.searchConfig.pageNum = res;
	await InitPageData.getListData();
}
