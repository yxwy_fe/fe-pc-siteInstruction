import pageData from '../data/pageData.js'
import DownLoadUtil from "../../../../utils/DownLoadUtil";
import {getDownloadFileApi} from '../apiBusiness/apiBusiness.js'

/**列表颜色绑定类名*/
export function tableRowClassNameBusiness({row}) {
	if (row.level === '3') {
		return "setClass";
	}
}

export function typeBusiness(val) {
	for (let item of pageData.type) {
		if (item.value === val) {
			return item.label;
		}
	}
}

/**文件标记*/
export function signTypeBusiness(val) {
	for (let item of pageData.signType) {
		if (item.value === val) {
			return item.label;
		}
	}
}

/**时分秒转换*/
export function getTimeBusiness(value) {
	if (value != 0) {
		let secondTime = parseInt(value); // 秒
		let minuteTime = 0; // 分
		let hourTime = 0; // 小时
		if (secondTime > 60) { // 如果秒数大于60，将秒数转换成整数
			// 获取分钟，除以60取整数，得到整数分钟
			minuteTime = parseInt(secondTime / 60);
			// 获取秒数，秒数取佘，得到整数秒数
			secondTime = parseInt(secondTime % 60);
			// 如果分钟大于60，将分钟转换成小时
			if (minuteTime > 60) {
				// 获取小时，获取分钟除以60，得到整数小时
				hourTime = parseInt(minuteTime / 60);
				// 获取小时后取佘的分，获取分钟除以60取佘的分
				minuteTime = parseInt(minuteTime % 60);
			}
		}
		let result
		// 秒数为0返回空
		if (secondTime > 0) {
			result = "" + parseInt(secondTime) + "秒";
		} else {
			result = "-"
		}
		if (minuteTime > 0) {
			result = "" + parseInt(minuteTime) + "分" + result;
		}
		if (hourTime > 0) {
			result = "" + parseInt(hourTime) + "时" + result;
		}
		return result
	} else {
		return '-'
	}
}

/**大小转换*/
export function bytesToSizeBusiness(limit) {
	let size = "";
	if (limit < 0.1 * 1024) {                            //小于0.1KB，则转化成B
		size = limit.toFixed(2) + "B"
	} else if (limit < 0.1 * 1024 * 1024) {            //小于0.1MB，则转化成KB
		size = (limit / 1024).toFixed(2) + "KB"
	} else if (limit < 0.1 * 1024 * 1024 * 1024) {        //小于0.1GB，则转化成MB
		size = (limit / (1024 * 1024)).toFixed(2) + "MB"
	} else {                                            //其余转化成GB
		size = (limit / (1024 * 1024 * 1024)).toFixed(2) + "GB"
	}

	let sizeStr = size + "";                        //转成字符串
	let index = sizeStr.indexOf(".");                    //获取小数点处的索引
	let dou = sizeStr.substr(index + 1, 2)            //获取小数点后两位的值
	if (dou == "00") {                                //判断后两位是否为00，若是是则删除00
		return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
	}
	return size;
}

/**拍摄时间失焦事件*/
export function pickerOneBusiness() {
	pageData.searchConfig.createTimeStart = pageData.shootTime[0];
	pageData.searchConfig.createTimeEnd = pageData.shootTime[1];
}

/**采集时间失焦事件*/
export function pickerTwoBusiness() {
	pageData.searchConfig.importTimeStart = pageData.gatherTime[0];
	pageData.searchConfig.importTimeEnd = pageData.gatherTime[1];
}

/**播放按钮*/
export function playBusiness(val) {
	pageData.formConfig.dialogTitle = '';
	pageData.formConfig.url = '';
	pageData.formConfig.type = '';
	let host = 'http://' + val.ip + '/' + val.videoPath + '/' + val.path;

	if (val.type === 'photo') {
		pageData.formConfig.isShow = true;
		pageData.formConfig.dialogTitle = '图片查看';
		pageData.formConfig.url = host;
		pageData.formConfig.type = val.type;
	} else if (val.type === 'video') {
		pageData.formConfig.isShow = true;
		pageData.formConfig.dialogTitle = '视频播放';
		pageData.formConfig.url = host;
		pageData.formConfig.type = val.type;
	} else if (val.type === 'audio') {
		pageData.formConfig.isShow = true;
		pageData.formConfig.dialogTitle = '音频播放';
		pageData.formConfig.url = host;
		pageData.formConfig.type = val.type;
	}

}

/**下载按钮*/
export async function downloadBusiness(val) {
	let _downLoadImageSrc = await getDownloadFileApi({id: val.id});
	DownLoadUtil.downLoadWithBlob(_downLoadImageSrc, val.fileName)
}


