import request from '@/utils/request';

// 音视频资料列表
export function getMediaListApi(data) {
	return request({
		url: '/system/media/list',
		method: 'get',
		params: data
	})
}


// 下载接口
export function getDownloadFileApi(data) {
	return request({
		url: '/system/media/downloadFile',
		responseType: "blob",
		method: 'get',
		params: data
	})
}
