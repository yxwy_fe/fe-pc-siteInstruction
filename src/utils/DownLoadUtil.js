/**
    createBy: muyufei
    time: 2024-01-15 11:24:10
    des: 下载工具
 */

class DownLoadUtil {
	/** 使用blob 流下载*/
	static downLoadWithBlob(imgSrc, name) {
		let blob = new Blob([imgSrc]);
		let url = window.URL.createObjectURL(blob); // 创建 url 并指向 blob

		let _link = document.createElement('a');
		_link.style.display = 'none';
		_link.href = url;
		_link.download = name;
		_link.click();

		window.URL.revokeObjectURL(url); // 释放该 url
	}
}

export default DownLoadUtil;
